package be.kdg.prog12.chart.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.*;
import javafx.scene.layout.BorderPane;

import java.text.DateFormatSymbols;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

public class LineChartView extends BorderPane {
	private LineChart<String, Number> lineChart;

	private static final Double[] MINIMA = {0.7, 0.7, 3.1, 5.3, 9.2, 11.9, 14.0, 13.6, 10.9, 7.8, 4.1,
		1.6};
	private static final Double[] MAXIMA = {5.7, 6.6, 10.4, 14.2, 18.1, 20.6, 23.0, 22.6, 19.0, 14.7,
		9.5, 6.1};

	public LineChartView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		lineChart.setTitle("Average temperatures by month, " +
			"Ukkel 1981-2010");
		setCenter(this.lineChart);
	}

	private void initialiseNodes() {
		lineChart = new LineChart<>(new CategoryAxis(), new NumberAxis());
		XYChart.Series<String, Number> series1 = new XYChart.Series<>();
		series1.setName("Average minima");
		XYChart.Series<String, Number> series2 = new XYChart.Series<>();
		series2.setName("Average maxima");
		for (int i = 0; i < MINIMA.length; i++) {
			final String month = Month.of(i + 1).getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
			series1.getData().add(new XYChart.Data<>(month, MINIMA[i]));
			series2.getData().add(new XYChart.Data<>(month, MAXIMA[i]));
		}
		lineChart.getData().addAll(series1, series2);
	}



}
