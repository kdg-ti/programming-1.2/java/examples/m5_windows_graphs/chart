package be.kdg.prog12.chart;

import be.kdg.prog12.chart.view.LineChartView;
import be.kdg.prog12.chart.view.PieChartView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class PieChartMain extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		PieChartView view = new PieChartView();
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}