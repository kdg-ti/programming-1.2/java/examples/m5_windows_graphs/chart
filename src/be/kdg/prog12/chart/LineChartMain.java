package be.kdg.prog12.chart;

import be.kdg.prog12.chart.view.LineChartView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LineChartMain extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		LineChartView view = new LineChartView();
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}